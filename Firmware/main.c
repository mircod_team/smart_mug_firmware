#include <stdbool.h>
#include <stdint.h>
#include <nrf_delay.h>
#include <nrf_gpio.h>
#include <SmartLed.h>

int main(void)
{
  nrf_gpio_cfg_output(NRF_GPIO_PIN_MAP(0, 13));
  
  SmartLed_Init(14);

  uint32_t counter_blink = 0;
  uint32_t counter_dim = 0;
  uint8_t light_level = 20;
  uint8_t direction = 0;
  
  nrf_delay_ms(3000);

  while(1)
  {
    if (counter_blink++ > 499)
    {
      nrf_gpio_pin_toggle(NRF_GPIO_PIN_MAP(0, 13));
      counter_blink = 0;
    }
    
    if(counter_dim++ > 1)
    {
            if (direction == 1)
      {
              light_level -= 3;
        if (light_level < 10)
        {
          direction = 0;
        }
      }
      else if (direction == 0)
      {
              light_level += 3;
        if (light_level > 250)
        {
          direction = 1;
        }
      }
      
      for (uint8_t i = 0; i < NR_OF_PIXELS; ++i)
      {
        SmartLed_SetPixelRgb(i, light_level, 0, light_level);
      }
      SmartLed_Show();
      counter_dim = 0;
    }
    nrf_delay_ms(1);
  }
}