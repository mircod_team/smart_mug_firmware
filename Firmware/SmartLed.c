#include "SmartLed.h"
#include <nrf_drv_pwm.h>
#include <nrf_delay.h>
#include <nrf_gpio.h>
#include <string.h>

#define RESET_ZEROS_AT_START    1280
#define PERIOD_TICKS            17
#define ONE_HIGH_TICKS          9
#define ZERO_HIGH_TICKS         4

static nrf_drv_pwm_t m_pwm0 = NRF_DRV_PWM_INSTANCE(0);

static nrf_pwm_values_common_t m_seq_values[NR_OF_PIXELS * 24 + RESET_ZEROS_AT_START + 1];
static nrf_pwm_sequence_t const m_seq =
{
		.values.p_common = m_seq_values,
		.length = NRF_PWM_VALUES_LENGTH(m_seq_values),
		.repeats = 0,
		.end_delay = 0
};

static smartLed_pixel_t pixels[NR_OF_PIXELS];

void SmartLed_Init(uint8_t pin)
{
	nrf_gpio_cfg_output(pin);
	nrf_gpio_pin_clear(pin);

	uint32_t err_code;
	nrf_drv_pwm_config_t const config0 =
	{
			.output_pins =
			{
					pin,
					NRF_DRV_PWM_PIN_NOT_USED,
					NRF_DRV_PWM_PIN_NOT_USED,
					NRF_DRV_PWM_PIN_NOT_USED
			},
			.irq_priority = APP_IRQ_PRIORITY_LOW,
			.base_clock = NRF_PWM_CLK_16MHz,
			.count_mode = NRF_PWM_MODE_UP,
			.top_value = PERIOD_TICKS,
			.load_mode = NRF_PWM_LOAD_COMMON,
			.step_mode = NRF_PWM_STEP_AUTO
	};

	err_code = nrf_drv_pwm_init(&m_pwm0, &config0, NULL);
	APP_ERROR_CHECK(err_code);

	for (int i = 0; i < NRF_PWM_VALUES_LENGTH(m_seq_values); i++)
	{
		m_seq_values[i] = ONE_HIGH_TICKS;
	}

	for (int i = 0; i < RESET_ZEROS_AT_START; i++)
	{
		m_seq_values[i] = 0x8000;
	}

	m_seq_values[NR_OF_PIXELS * 24 + RESET_ZEROS_AT_START] = 0x8000;

	nrf_drv_pwm_simple_playback(&m_pwm0, &m_seq, 1, 0);
}


void SmartLed_SetPixelRgb(uint8_t pixel_nr, uint8_t red, uint8_t green, uint8_t blue)
{
	pixels[pixel_nr].red = red;
	pixels[pixel_nr].green = green;
	pixels[pixel_nr].blue = blue;
}

void SmartLed_SetPixel(uint8_t pixel_nr, smartLed_pixel_t* color)
{
	memcpy(&pixels[pixel_nr], color, sizeof(smartLed_pixel_t));
}

void SmartLed_Show(void)
{
	for (uint8_t i = 0; i < (sizeof(pixels) / sizeof(smartLed_pixel_t)); i++)
	{
		for (uint8_t j = 0; j < 8; j++)
		{
			if ((pixels[i].green << j) & 0x80)
			{
				m_seq_values[RESET_ZEROS_AT_START + i * 24 + j] = ONE_HIGH_TICKS | 0x8000;
			}
			else
			{
				m_seq_values[RESET_ZEROS_AT_START + i * 24 + j] = ZERO_HIGH_TICKS | 0x8000;
			}
		}
		for (uint8_t j = 0; j < 8; j++)
		{
			if ((pixels[i].red << j) & 0x80)
			{
				m_seq_values[RESET_ZEROS_AT_START + i * 24 + j + 8] = ONE_HIGH_TICKS | 0x8000;
			}
			else
			{
				m_seq_values[RESET_ZEROS_AT_START + i * 24 + j + 8] = ZERO_HIGH_TICKS | 0x8000;
			}
		}
		for (uint8_t j = 0; j < 8; j++)
		{
			if ((pixels[i].blue << j) & 0x80)
			{
				m_seq_values[RESET_ZEROS_AT_START + i * 24 + j + 16] = ONE_HIGH_TICKS | 0x8000;
			}
			else
			{
				m_seq_values[RESET_ZEROS_AT_START + i * 24 + j + 16] = ZERO_HIGH_TICKS | 0x8000;
			}
		}
	}

	nrf_drv_pwm_simple_playback(&m_pwm0, &m_seq, 1, 0);
}