#ifndef SMARTLED_H
#define SMARTLED_H

#include <stdint.h>

#define NR_OF_PIXELS 6

typedef struct
{
	uint8_t red;
	uint8_t green;
	uint8_t blue;
} smartLed_pixel_t;

void SmartLed_Init(uint8_t pin);
void SmartLed_SetPixelRgb(uint8_t pixel_nr, uint8_t red, uint8_t green, uint8_t blue);
void SmartLed_SetPixel(uint8_t pixel_nr, smartLed_pixel_t* color);
void SmartLed_Show(void);

#endif // !SMARTLED_H